﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataLakkireddy.Models
{
    public class ShoppingMall
    {
            [Key]
            [ScaffoldColumn(false)]
            public int MallID { get; set; }


            [Display(Name = "MallName")]
            public string MallName { get; set; }

            [Display(Name = "Email Id")]
            public String EmaidId { get; set; }

            [RegularExpression(@"^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$")]
            [Display(Name = "Phone Number")]
            public String PhoneNumber { get; set; }

        [Display(Name = "Place")]
        public String Place { get; set; }

        [Display(Name = "Year Established")]
            public int YearEstablished { get; set; }

            [RegularExpression("^ http(s) ?://([\\w-]+.)+[\\w-]+(/[\\w- ./?%&=])?$")]
            [Display(Name = "Website")]
            public String WebsiteURL { get; set; }

        public List<Location> filmSets { get; set; }

        public static List<ShoppingMall> ReadAllFromCSV(string filepath)
        {
            List<ShoppingMall> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => ShoppingMall.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static ShoppingMall OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            ShoppingMall item = new ShoppingMall();

            int i = 0;
           
            //item.MallID = Convert.ToInt32(values[i++]);
            item.MallName = Convert.ToString(values[i++]);
            item.EmaidId = Convert.ToString(values[i++]);
            item.PhoneNumber = Convert.ToString(values[i++]);
            item.Place = Convert.ToString(values[i++]);
            item.YearEstablished = Convert.ToInt32(values[i++]);
            item.WebsiteURL = Convert.ToString(values[i++]);


            return item;
        }
    }
}
