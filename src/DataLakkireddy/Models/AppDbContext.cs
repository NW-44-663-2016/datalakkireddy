﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataLakkireddy.Models
{
    public class AppDbContext : DbContext
    {

        public DbSet<Location> Locations { get; set; }
        public DbSet<ShoppingMall>ShoppingMalls { get; set; }
    }
}

