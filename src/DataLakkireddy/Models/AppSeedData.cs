﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataLakkireddy.Models
{
    public static class AppSeedData
    {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.ShoppingMalls.RemoveRange(context.ShoppingMalls);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedShoppingMallFromCsv(relPath, context); 
        }

        //    context.Locations.RemoveRange(context.Locations);
        //    new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
        //    new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
        //    new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
        //    new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
        //    new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
        //    new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };


            //    context.ShoppingMall.RemoveRange(context.ShoppingMall);
            //    new ShoppingMall() { MallName = "Walmart", EmaidId = "walmart@gmail.com", PhoneNumber = " 4537654290", Place = "Maryville", YearEstablished = 1970, WebsiteURL = "http://www.walmart.com/" };
            //    new ShoppingMall() { MallName = "JCPenney", EmaidId = "jcpenney@gmail.com", PhoneNumber = " 7653901954", Place = "Denver", YearEstablished = 1985, WebsiteURL = "http://www.jcpenney.com/" };
            //    new ShoppingMall() { MallName = "Rue 21", EmaidId = "rue 21@gmail.com", PhoneNumber = " 2074370841", Place = "Orlando", YearEstablished = 1976, WebsiteURL = "http://www.rue21.com/" };
            //    new ShoppingMall() { MallName = "Aeropostale", EmaidId = "aeropostale@gmail.com", PhoneNumber = " 5024860435", Place = "Maryville", YearEstablished = 1973, WebsiteURL = "http://www.aeropostale.com/" };
            //    new ShoppingMall() { MallName = "Hollister", EmaidId = "hollister@gmail.com", PhoneNumber = " 6693450254", Place = "Maryville", YearEstablished = 2000, WebsiteURL = "http://www.hollister.com/" };
            //    new ShoppingMall() { MallName = "Gap", EmaidId = "gap@gmail.com", PhoneNumber = " 9458720467", Place = "Sanfrancisco", YearEstablished = 1969, WebsiteURL = "http://www.gap.com/" };
            //    new ShoppingMall() { MallName = "H & M", EmaidId = "H&M@gmail.com", PhoneNumber = " 8437601635", Place = "Maryville", YearEstablished = 1981, WebsiteURL = "http://www.H&M.com/" };


            //  context.SaveChanges();
            //    SeedLocationsFromCsv(relPath, context);
            //    SeedShoppingMallFromCsv(relPath, context);
            //}



        private static void SeedShoppingMallFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "shoppingMall.csv";
            if (!File.Exists(source)) { throw new Exception("Cannot find file " + source); }
            ShoppingMall.ReadAllFromCSV(source);
            List<ShoppingMall> lst = ShoppingMall.ReadAllFromCSV(source);
            context.ShoppingMalls.AddRange(lst.ToArray());
            context.SaveChanges();

        }

        private static void SeedLocationsFromCsv(object relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }


    }

}