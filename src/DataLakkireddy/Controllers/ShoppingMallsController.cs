using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataLakkireddy.Models;

namespace DataLakkireddy.Controllers
{
    public class ShoppingMallsController : Controller
    {
        private AppDbContext _context;

        public ShoppingMallsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: ShoppingMalls
        public IActionResult Index()
        {
            return View(_context.ShoppingMalls.ToList());
        }

        // GET: ShoppingMalls/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);
            if (shoppingMall == null)
            {
                return HttpNotFound();
            }

            return View(shoppingMall);
        }

        // GET: ShoppingMalls/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ShoppingMalls/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ShoppingMall shoppingMall)
        {
            if (ModelState.IsValid)
            {
                _context.ShoppingMalls.Add(shoppingMall);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shoppingMall);
        }

        // GET: ShoppingMalls/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);
            if (shoppingMall == null)
            {
                return HttpNotFound();
            }
            return View(shoppingMall);
        }

        // POST: ShoppingMalls/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(ShoppingMall shoppingMall)
        {
            if (ModelState.IsValid)
            {
                _context.Update(shoppingMall);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(shoppingMall);
        }

        // GET: ShoppingMalls/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);
            if (shoppingMall == null)
            {
                return HttpNotFound();
            }

            return View(shoppingMall);
        }

        // POST: ShoppingMalls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            ShoppingMall shoppingMall = _context.ShoppingMalls.Single(m => m.MallID == id);
            _context.ShoppingMalls.Remove(shoppingMall);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
