using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataLakkireddy.Models;

namespace DataLakkireddy.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222050555_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataLakkireddy.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<int?>("ShoppingMallMallID");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataLakkireddy.Models.ShoppingMall", b =>
                {
                    b.Property<int>("MallID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EmaidId");

                    b.Property<string>("MallName");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("Place");

                    b.Property<string>("WebsiteURL");

                    b.Property<int>("YearEstablished");

                    b.HasKey("MallID");
                });

            modelBuilder.Entity("DataLakkireddy.Models.Location", b =>
                {
                    b.HasOne("DataLakkireddy.Models.ShoppingMall")
                        .WithMany()
                        .HasForeignKey("ShoppingMallMallID");
                });
        }
    }
}
